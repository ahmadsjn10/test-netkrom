<?php

namespace App\Http\Controllers;

use App\Formulir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
class PublicController extends Controller
{
    public function form()
    {
        $formulir = new Formulir();

        return view('form', compact('formulir'));
    }

    public function formStore(Request $request)
    {
        $this->validator()->validate();
        DB::beginTransaction();
        try {
            $data['name'] = $request->nama;
            $data['no_tlp'] = $request->no_telpon;
            $data['email'] = $request->email;
            $formulir = Formulir::create($data);

            DB::commit();
            return redirect()->route('thank', [$formulir]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function thank(Request $request, $id)
    {
        $formulir = Formulir::find($id);

        return view('thank-you', compact('formulir'));
    }

    /**
     * Get create and update validator
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator()
    {
        $payload = request()->only([
            'nama',
            'email',
            'no_telpon'
        ]);

        return Validator::make($payload, [
            'nama' => 'required|max:255',
            'email' => 'required|unique:users,email',
            'no_telpon' => 'required'
        ]);
    }
}
