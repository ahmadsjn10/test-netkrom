<?php

namespace App\Http\Controllers;

use App\Formulir;
use Illuminate\Http\Request;
use Validator;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if(!empty($request->status)){
            if($request->status == 'belum'){
                $formulirs = Formulir::where('is_valid', 0)->paginate(5);    
            }else{
                $formulirs = Formulir::where('is_valid', 1)->paginate(5);
            }
        }else{
            $formulirs = Formulir::paginate(5);
        }


        return view('home', compact('formulirs'));
    }

    public function editFormulir(Request $request, $id)
    {
        $formulir = Formulir::find($id);

        return view('form', compact('formulir'));
    }
    
    public function updateFormulir(Request $request, $id)
    {
        $formulir = Formulir::find($id);
        $this->validator()->validate();
        DB::beginTransaction();
        try {
            $data['name'] = $request->nama;
            $data['no_tlp'] = $request->no_telpon;
            $data['email'] = $request->email;
            $formulir->update($data);

            DB::commit();
            return redirect()->route('home');
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function deleteFormulir(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $formulir = Formulir::find($id);
            $formulir->delete();
            DB::commit();
            return redirect()->route('home');
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
     
    public function checkIn(){
        return view('check-in');
    }

    public function checkInStore(Request $request){
        DB::beginTransaction();
        try {
            $formulir = Formulir::where('code', $request->code)->first();
            if(empty($formulir)){
                return redirect()->back()->with("error", "Maaf code yang anda masukan salah");
            }else{
                if($formulir->is_valid == 1){
                    return redirect()->back()->with("error", "Maaf code yang anda masukan telah di gunakan");
                }else{
                    $formulir->is_valid = 1;
                    $formulir->save();
                    DB::commit();

                    return redirect()->back()->with("success", "Yes Berhasil, Penonton dipersilahkan masuk vanue konser");
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }
    
    /**
     * Get create and update validator
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator()
    {
        $payload = request()->only([
            'nama',
            'email',
            'no_telpon'
        ]);

        return Validator::make($payload, [
            'nama' => 'required|max:255',
            'email' => 'required|unique:users,email',
            'no_telpon' => 'required'
        ]);
    }
}
