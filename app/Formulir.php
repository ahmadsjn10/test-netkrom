<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formulir extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'code',
        'is_valid',
        'no_tlp'
    ];

        /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        static::saved(
            function (Formulir $formulir) {
                if (empty($formulir->code)) {
                    $key = $formulir->getKey();

                    $formulirDate = $formulir->getAttribute('created_at');

                    if (!$formulirDate instanceof \DateTimeInterface) {
                        $formulirDate = \Illuminate\Support\Carbon::createFromFormat("Y-m-d H:i:s", $formulirDate);
                    }

                    $id = 'ID';

                    $formulir->setAttribute("code", sprintf("%s/%s/%s", $key, $id, $formulirDate->format("d/m/Y")));

                    $formulir->save();
                }
            }
        );
    }

}
