@extends('welcome')
@section('content')
    <div class="card-header">
        Formulir
    </div>
    <form class="card-body" method="POST" action="{{ $formulir->getKey() ? route('formulir.update', [$formulir]) : route('form.store') }}">
        @csrf
        <div class="form-group row">
            <label for="nama" class="col-md-3">Nama</label>
            <div class="col-md-6">
                <input class="form-control {{ $errors->has('nama') ? ' is-invalid' : '' }}" type="text" name="nama" id="nama" value="{{ old('nama') ?? $formulir->name ?? ''}}" >
                @if ($errors->has('nama'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('nama') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-md-3">Email</label>
            <div class="col-md-6">
                <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email') ?? $formulir->email ?? ''}}" >
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="no_telpon" class="col-md-3">No Telpon</label>
            <div class="col-md-6">
                <input class="form-control {{ $errors->has('no_telpon') ? ' is-invalid' : '' }}" type="text" name="no_telpon" id="no_telpon" value="{{ old('no_telpon') ?? $formulir->no_tlp ?? ''}}" >
                @if ($errors->has('no_telpon'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('no_telpon') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="button button-group-form text-center">
            <button type="submit" class="btn btn-md btn-primary">
                {{$formulir->getKey() ? "UPDATE": "BUAT"}}
            </button>
        </div>
    </form>
@endsection