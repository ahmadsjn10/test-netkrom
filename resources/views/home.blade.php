@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Datar Penonton (Jumlah : {{$formulirs->count()}})
                    <input type="hidden" id="url-home" value="{{route("home")}}">
                    <select id="checkStatus" class="col-md-4 form-control float-right" onclick="window.location.href = $('#url-home').val()+'?status='+$('#checkStatus').val()">
                        <option value="">Pilih Status</option>
                        <option value="belum">Belum Check In</option>
                        <option value="sudah">Sudah Check In</option>
                    </select>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">No Telpon</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($formulirs as $key => $formulir)
                                <tr>
                                    <th scope="row">{{ $key+1 }}</th>
                                    <td>{{ $formulir->name }}</td>
                                    <td>{{ $formulir->email }}</td>
                                    <td>{{ $formulir->no_tlp }}</td>
                                    <td>{{ $formulir->is_valid ? "sudah check-in": "belum check-in"}}</td>
                                    <td>
                                        <a class="btn btn-primary" href="{{route('formulir.edit', [$formulir])}}">Edit</a>
                                        <a class="btn btn-danger" href="{{route('formulir.delete', [$formulir])}}">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $formulirs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
