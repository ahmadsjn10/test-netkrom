@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Check ID</div>
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                    {{ Session::get('error')}}
                    </div>
                @endif

                @if(Session::has('success'))
                    <div class="alert alert-success">
                    {{ Session::get('success')}}
                    </div>
                @endif

                <form class="card-body" action="{{route('checkIn.store')}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <label for="code" class="col-md-3">Masukan Kode ID</label>
                        <div class="col-md-6">
                            <input class="form-control {{ $errors->has('code') ? ' is-invalid' : '' }}" type="text" name="code" id="code" value="{{ old('code') }}" required>
                            @if ($errors->has('code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="button button-group-form text-center">
                        <button type="submit" class="btn btn-md btn-primary">
                            Check-In
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
