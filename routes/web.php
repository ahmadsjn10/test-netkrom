<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PublicController@form')->name('form');
Route::post('/', 'PublicController@formStore')->name('form.store');
Route::get('/thank-you/{id}', 'PublicController@thank')->name('thank');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/check-in', 'HomeController@checkIn')->name('check-in');
Route::post('/check-in', 'HomeController@checkInStore')->name('checkIn.store');
Route::get('/edit/{id}', 'HomeController@editFormulir')->name('formulir.edit');
Route::post('/update/{id}', 'HomeController@updateFormulir')->name('formulir.update');
Route::get('/destroy/{id}', 'HomeController@deleteFormulir')->name('formulir.delete');
